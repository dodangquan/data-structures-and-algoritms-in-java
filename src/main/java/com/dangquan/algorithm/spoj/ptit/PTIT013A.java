package com.dangquan.algorithm.spoj.ptit;

import java.util.Scanner;

/**
 * @author dangquanzt@gmail.com
 */
public class PTIT013A {

	public static boolean isSomayman86(int number) {
		if (number % 100 == 86) {
			return true;
		}
		return false;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int[] arr = new int[n];
		for (int i = 0; i < n; i++) {
			if (isSomayman86(sc.nextInt())) {
				arr[i] = 1;
			} else {
				arr[i] = 0;
			}
		}
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i]);
			if (i < arr.length - 1) {
				System.out.println();
			}
		}
		sc.close();
	}
}
