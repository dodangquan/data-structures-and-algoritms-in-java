package com.dangquan.algorithm.spoj.ptit;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author dangquanzt@gmail.com
 */
public class BCSINH {

	private static boolean flag = false;
	private static ArrayList<Integer> list;

	private static void init(int length) {
		list = new ArrayList<>(length);
		for (int i = 0; i < length; i++) {
			list.add(0);
		}
	}

	private static void show(ArrayList<Integer> list) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < list.size(); i++) {
			sb.append(list.get(i));
		}
		System.out.println(sb.toString());
	}

	private static void nextBitString(ArrayList<Integer> list) {
		int i = list.size() - 1;
		while (i >= 0 && list.get(i) == 1) {
			list.set(i, 0);
			i--;
		}
		if (i < 0) {
			flag = true;
		} else {
			list.set(i, 1);
		}
	}

	private static void generate(ArrayList<Integer> list) {
		while (!flag) {
			show(list);
			nextBitString(list);
		}
	}

	public static void main(String[] args) throws java.lang.Exception {
		Scanner sc = new Scanner(System.in);
		int length = sc.nextInt();
		init(length);
		generate(list);
		sc.close();
	}
}