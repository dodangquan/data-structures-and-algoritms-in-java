package com.dangquan.algorithm.spoj.ptit;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author dangquanzt@gmail.com
 */
public class BCTOHOP {

	private static boolean flag = false;
	private static ArrayList<Integer> list;

	public static void init(int k) {
		list = new ArrayList<>(k);
		for (int i = 0; i < k; i++) {
			list.add(i);
		}
	}

	public static void show(ArrayList<Integer> list) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < list.size(); i++) {
			sb.append(list.get(i));
			if (i < list.size() - 1) {
				sb.append(" ");
			}
		}
		System.out.println(sb.toString());
	}

	public static void nextCombination(int n, int k) {
		int i = k - 1;
		while (i >= 0 && list.get(i) == n - k + i) {
			i--;
		}
		if (i >= 0) {
			list.set(i, list.get(i) + 1);
			for (int j = i + 1; j < k; j++) {
				list.set(j, list.get(i) + j - i);
			}
		} else {
			flag = true;
		}
	}

	public static void combination(int n, int k) {
		flag = false;
		while (!flag) {
			show(list);
			nextCombination(n, k);
		}
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int k = sc.nextInt();
		init(k);
		combination(n, k);
		sc.close();
	}
}
