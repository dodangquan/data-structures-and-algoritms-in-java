package com.dangquan.algorithm.spoj.ptit;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;
import java.util.Scanner;

/**
 * @author dangquanzt@gmail.com
 */
public class BCSTACK {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		ArrayList<Integer> theStack = new ArrayList<>();
		Queue<String> stringQueue = new ArrayDeque<>();
		boolean flag = false;
		while (true) {
			String c = sc.next();
			switch (c) {
				case "init":
					theStack = new ArrayList<>();
					break;
				case "push":
					int num = sc.nextInt();
					theStack.add(num);
					break;
				case "pop":
					if (!theStack.isEmpty()) {
						theStack.remove(theStack.size() - 1);
					}
					break;
				case "top":
					if (theStack.isEmpty()) {
						stringQueue.add("-1");
					} else {
						stringQueue.add(theStack.get(theStack.size() - 1) + "");
					}
					break;
				case "size":
					stringQueue.add(theStack.size() + "");
					break;
				case "empty":
					if (theStack.isEmpty()) {
						stringQueue.add("1");
					} else {
						stringQueue.add("0");
					}
					break;
				case "end":
					flag = true;
					break;
				default:
					flag = true;
					break;
			}
			if (flag) {
				break;
			}
		}
		while (!stringQueue.isEmpty()) {
			System.out.println(stringQueue.poll());
		}
		sc.close();
	}
}
