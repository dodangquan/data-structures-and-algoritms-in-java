package com.dangquan.algorithm.spoj.ptit;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

/**
 * @author dangquanzt@gmail.com
 */
public class REMOVBIT {

	public static long heNhiphanHeThapphan(String nhiphanStr) {
		long s = 0;
		for (int i = 0; i < nhiphanStr.length(); i++) {
			s = s + (long) (Long.parseLong(nhiphanStr.charAt(i) + "")
					* Math.pow(2, nhiphanStr.length() - 1 - i));
		}
		return s;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		ArrayList<Character> list = new ArrayList<>();
		String str = sc.next();
		for (int i = 0; i < str.length(); i++) {
			list.add(str.charAt(i));
		}
		boolean kt = false;
		Iterator<Character> iterator = list.iterator();
		while (iterator.hasNext()) {
			char ch = iterator.next();
			if (ch == '0') {
				kt = true;
				iterator.remove();
				break;
			}
		}
		if (!kt){
			iterator.remove();
		}
		iterator = list.iterator();
		while (iterator.hasNext()) {
			System.out.print(iterator.next());
		}
		sc.close();
	}
}
