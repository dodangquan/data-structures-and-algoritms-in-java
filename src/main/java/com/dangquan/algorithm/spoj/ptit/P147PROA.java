package com.dangquan.algorithm.spoj.ptit;

import java.util.Scanner;

/**
 * @author dangquanzt@gmail.com
 */
public class P147PROA {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int ab = sc.nextInt();
		int ac = sc.nextInt();
		int bc = sc.nextInt();
		System.out.println(4 *
				(long) (Math.sqrt(ab * ac / bc) + Math.sqrt(bc * ab / ac)
						+ Math.sqrt(ac * bc / ab)));
		sc.close();
	}
}
