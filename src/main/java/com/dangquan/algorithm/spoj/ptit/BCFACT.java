package com.dangquan.algorithm.spoj.ptit;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author dangquanzt@gmail.com
 */
public class BCFACT {

	public static long getFactorial(int num) {
		if (num == 0 || num == 1) {
			return 1;
		} else {
			return num * getFactorial(num - 1);
		}
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		ArrayList<Integer> list = new ArrayList<>();
		while (true) {
			int i = sc.nextInt();
			if (i == 0) {
				break;
			}
			list.add(i);
		}
		for (Integer k : list) {
			System.out.println(getFactorial(k));
		}
		sc.close();
	}
}
