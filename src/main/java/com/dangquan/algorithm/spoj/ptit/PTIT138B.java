package com.dangquan.algorithm.spoj.ptit;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;

/**
 * @author dangquanzt@gmail.com
 */
public class PTIT138B {

	static HashMap<String, Integer> map = new HashMap<>();

	static class Information {
		private String fatherName;
		private Integer fatherAge;
		private Integer fatherAgeOfPass;
		private String sonName;
		private Integer sonAge = null;

		public Information() {
			super();
		}

		public Information(String fatherName, String sonName, int fatherAgeOfPass) {
			Information.this.fatherName = fatherName;
			Information.this.sonName = sonName;
			Information.this.fatherAgeOfPass = fatherAgeOfPass;
			if (map.containsKey(Information.this.fatherName)
					&& map.get(Information.this.fatherName) != null) {
				Information.this.fatherAge = map.get(Information.this.fatherName);
				Information.this.sonAge = Information.this.fatherAge
						- Information.this.fatherAgeOfPass;
				map.put(Information.this.sonName, Information.this.sonAge);
			} else {
				map.put(Information.this.sonName, null);
			}

		}

		@Override
		public String toString() {
			return sonName + " " + sonAge;
		}
	}

	public static void main(String[] args) {
		map.put("Ted", 100);
		Scanner sc = new Scanner(System.in);
		Comparator<Information> comparator = new PTIT138B.MyComparator();
		Queue<String> arrayQueue = new ArrayDeque<>();
		Queue<Information> queue = new PriorityQueue<>(comparator);
		ArrayList<Information> list = new ArrayList<>();
		int numberOfTestCases = sc.nextInt();
		int numberOfPersons;
		String fatherName;
		String sonName;
		int fatherAgeOfPass;
		Information info;
		Iterator<Information> iterator;
		int i = 1;
		while (numberOfTestCases > 0) {
			arrayQueue.add("DATASET " + i);
			i++;
			queue = new PriorityQueue<>(comparator);
			list = new ArrayList<>();
			map = new HashMap<>();
			map.put("Ted", 100);
			numberOfTestCases--;
			numberOfPersons = sc.nextInt();
			while (numberOfPersons > 0) {
				numberOfPersons--;
				fatherName = sc.next();
				sonName = sc.next();
				fatherAgeOfPass = sc.nextInt();
				info = new Information(fatherName, sonName, fatherAgeOfPass);
				list.add(info);
			}
			int count = 0;
			boolean kt = true;
			while (kt) {
				count = 0;
				iterator = list.iterator();
				while (iterator.hasNext()) {
					Information a = iterator.next();
					if (map.containsKey(a.fatherName) && map.get(a.fatherName) != null) {
						a.sonAge = map.get(a.fatherName) - a.fatherAgeOfPass;
						map.put(a.sonName, a.sonAge);
					} else {
						count++;
					}
				}
				if (count == 0) {
					kt = false;
				}
			}
			iterator = list.iterator();
			while (iterator.hasNext()) {
				Information t = iterator.next();
				queue.add(t);
			}
			while (!queue.isEmpty()) {
				arrayQueue.add(queue.poll().toString());
			}
		}
		Iterator<String> it = arrayQueue.iterator();
		while (it.hasNext()) {
			System.out.println(it.next());
		}
		sc.close();
	}

	static class MyComparator implements Comparator<Information> {
		@Override
		public int compare(Information o1, Information o2) {
			if (o1.sonAge < o2.sonAge) {
				return 1;
			} else if (o1.sonAge > o2.sonAge) {
				return -1;
			} else {
				return o1.sonName.compareToIgnoreCase(o2.sonName);
			}
		}
	}
}
