package com.dangquan.algorithm.spoj.ptit;

import java.math.BigInteger;
import java.util.Scanner;

/**
 * @author dangquanzt@gmail.com
 */
public class SUMSQR1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		BigInteger bigN = new BigInteger(String.valueOf(n));
		BigInteger big2 = new BigInteger("2");
		BigInteger big6 = new BigInteger("6");
		BigInteger result = BigInteger.ONE;
		// n * (n + 1) * (2 * n + 1) / 6
		result = bigN.multiply(bigN.add(BigInteger.ONE))
				.multiply(big2.multiply(bigN).add(BigInteger.ONE)).divide(big6);
		System.out.print(result);
		sc.close();
	}
}
