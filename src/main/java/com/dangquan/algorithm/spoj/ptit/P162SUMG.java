package com.dangquan.algorithm.spoj.ptit;

import java.util.Scanner;

/**
 * @author dangquanzt@gmail.com
 */
public class P162SUMG {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String str = sc.nextLine();
		int thuong = 0;
		int hoa = 0;
		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) >= 'a' && str.charAt(i) <= 'z') {
				thuong++;
			} else {
				hoa++;
			}
		}
		if (hoa > thuong) {
			System.out.print(str.toUpperCase());
		} else {
			System.out.print(str.toLowerCase());
		}
		sc.close();
	}
}
