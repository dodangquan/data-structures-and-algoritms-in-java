package com.dangquan.algorithm.spoj.ptit;

import java.util.Scanner;

/**
 * @author dangquanzt@gmail.com
 */
public class P152SUMG {

	public static boolean isPrime(int num) {
		if (num <= 1) {
			return false;
		} else {
			for (int i = 2; i <= Math.sqrt(num); i++) {
				if (num % i == 0) {
					return false;
				}
			}
			return true;
		}
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int x = sc.nextInt();
		int y = sc.nextInt();
		if (x >= y) {
			System.out.print("NO");
		} else if (x == 2 && y == 3) {
			System.out.print("YES");
		} else if (!isPrime(y)) {
			System.out.print("NO");
		} else {
			for (int i = x + 1; i < y; i++) {
				if (isPrime(i)) {
					System.out.print("NO");
					sc.close();
					return;
				}
			}
			System.out.print("YES");
		}
		sc.close();
	}
}
