package com.dangquan.algorithm.spoj.ptit;

import java.io.BufferedInputStream;
import java.util.ArrayDeque;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/**
 * @author dangquanzt@gmail.com
 */
public class BCACM11D {

	public static void main(String[] args) {
		Scanner sc = new Scanner(new BufferedInputStream(System.in));
		int numberOfTestCase = sc.nextInt();
		boolean[] isVisited;
		int[] step;
		int start;
		int end;
		LinkedList<Integer> queue;
		Queue<Integer> queue2 = new ArrayDeque<>();
		Queue<Integer> queue3 = new ArrayDeque<>(numberOfTestCase);
		for (int i = 0; i < numberOfTestCase; i++) {
			queue2.add(sc.nextInt());
			queue2.add(sc.nextInt());
		}
		for (int i = 1; i <= numberOfTestCase; i++) {
			start = queue2.poll();
			end = queue2.poll();
			isVisited = new boolean[10000];
			step = new int[10000];
			queue = new LinkedList<>();
			queue.addLast(start);
			isVisited[start] = true;
			while (!queue.isEmpty()) {
				int current = queue.pop();
				if (current == end) {
					break;
				}
				for (int j = 0; j <= 9; j++) {
					int next1 = getNext(1, j, current);
					int next2 = getNext(2, j, current);
					int next3 = getNext(3, j, current);
					int next4 = getNext(4, j, current);
					if (!isVisited[next1]) {
						queue.addLast(next1);
						step[next1] = step[current] + 1;
						isVisited[next1] = true;
					}
					if (!isVisited[next2]) {
						queue.addLast(next2);
						step[next2] = step[current] + 1;
						isVisited[next2] = true;
					}
					if (!isVisited[next3]) {
						queue.addLast(next3);
						step[next3] = step[current] + 1;
						isVisited[next3] = true;
					}
					if (!isVisited[next4]) {
						queue.addLast(next4);
						step[next4] = step[current] + 1;
						isVisited[next4] = true;
					}
				}
			}
			queue3.add(step[end]);
		}
		while (!queue3.isEmpty()) {
			System.out.print(queue3.poll());
			if (queue3.size() > 0) {
				System.out.println();
			}
		}
		sc.close();
	}

	private static int getNext(int flag, int i, int current) {
		int next = 0;
		int t;
		switch (flag) {
			case 1:
				if (i == 0) {
					return current;
				}
				next = current % 1000 + i * 1000;
				break;
			case 2:
				t = current / 1000;
				next = t * 1000 + current % 1000 % 100 + i * 100;
				break;
			case 3:
				t = current / 100;
				int tt = current % 10;
				next = t * 100 + i * 10 + tt;
				break;
			case 4:
				next = current / 10 * 10 + i;
				break;
			default:
				break;
		}
		if (!isPrime(next)) {
			return current;
		}
		return next;
	}
	
	private static boolean isPrime(int num) {
		if (num <= 1) {
			return false;
		} else {
			for (int i = 2; i <= Math.sqrt(num); i++) {
				if (num % i == 0) {
					return false;
				}
			}
			return true;
		}
	}
}
