package com.dangquan.algorithm.spoj.ptit;

import java.io.BufferedInputStream;
import java.util.Scanner;

/**
 * @author dangquanzt@gmail.com
 */
public class ALGOPRO7 {

	public static void quickSort(int[] a, int low, int high) {
		if (a == null || a.length == 0) {
			return;
		}
		if (low >= high) {
			return;
		}
		int middle = (low + high) / 2;
		int p = a[middle];

		int i = low;
		int j = high;

		while (i <= j) {
			while (a[i] < p) {
				i++;
			}
			while (a[j] > p) {
				j--;
			}
			if (i <= j) {
				swap(a, i, j);
				i++;
				j--;
			}
		}
		if (low < j) {
			quickSort(a, low, j);
		}
		if (i < high) {
			quickSort(a, i, high);
		}
	}

	public static void swap(int[] a, int i, int j) {
		int temp = a[i];
		a[i] = a[j];
		a[j] = temp;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(new BufferedInputStream(System.in));
		int n = sc.nextInt();
		int k = sc.nextInt();
		int[] a = new int[n];
		for (int i = 0; i < n; i++) {
			 a[i] = sc.nextInt();
		}
		quickSort(a, 0, a.length - 1);
		System.out.print(a[k]);
		sc.close();
	}
}
