package com.dangquan.algorithm.spoj.ptit;

import java.util.Scanner;

/**
 * @author dangquanzt@gmail.com
 */
public class PTIT125I {
	public static void removeNumber(int k, String str, int t) {
		if (k == 0) {
			System.out.print(str.substring(t));
		} else {
			int c = t;
			char q = str.charAt(c);
			for (int i = t + 1; i <= t + k; i++) {
				if (q < str.charAt(i)) {
					q = str.charAt(i);
					c = i;
				}
				if (q == '9') {
					break;
				}
			}
			System.out.print(q);
			removeNumber(k - (c - t), str, c + 1);
		}
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		sc.nextInt();
		int k = sc.nextInt();
		String str = sc.next();
		if (k < str.length()) {
			removeNumber(k, str, 0);
		}
		sc.close();
	}
}
