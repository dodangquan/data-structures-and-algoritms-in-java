package com.dangquan.algorithm.spoj.ptit;

import java.util.Scanner;

/**
 * @author dangquanzt@gmail.com
 */
public class BCFACTOR {
	public static boolean isPrime(int num) {
		if (num <= 1) {
			return false;
		} else {
			for (int i = 2; i <= Math.sqrt(num); i++) {
				if (num % i == 0) {
					return false;
				}
			}
			return true;
		}
	}

	public static void main(String[] args) throws Exception {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		if (isPrime(n)) {
			System.out.print(n + " " + 1);
		} else {
			int i = 2;
			int count = 0;
			while (n != 1) {
				if (isPrime(i) && n % i == 0) {
					count++;
					n = n / i;
				}
				if (count > 0 && n % i != 0) {
					System.out.println(i + " " + count);
					count = 0;
				} else if (count == 0) {
					i++;
				}
			}
//			dequi(n, 2, count);
		}
		sc.close();
	}

	public static void dequi(int n, int i, int count) {
		if (n == 1) {
			return;
		} else if (isPrime(i)) {
			if (n % i == 0) {
				count++;
				n = n / i;
			}
			if (count == 0) {
				++i;
			} else if (count > 0 && n % i != 0) {
				System.out.println(i + " " + count); // In ra console
				++i;
				count = 0;
			}
		} else {
			i++;
		}
		dequi(n, i, count);
	}
}
