package com.dangquan.algorithm.spoj.ptit;

import java.util.Scanner;

/**
 * @author dangquanzt@gmail.com
 */
public class BCFIBO {

	@SuppressWarnings("unused")
	private static int fibonacci(int n) {
		if (n == 0) {
			return 0;
		}
		if (n == 1) {
			return 1;
		}
		return fibonacci(n - 1) + fibonacci(n - 2);
	}

	private static long betterFibonacciDP(int num) {
		long n1 = 1;
		long n2 = 1;
		long current = 1;
		for (int i = 3; i <= num; i++) {
			current = n1 + n2;
			n2 = n1;
			n1 = current;
		}
		return current;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print(betterFibonacciDP(sc.nextInt()));
		sc.close();
	}
}
