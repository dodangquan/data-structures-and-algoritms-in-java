package com.dangquan.algorithm.spoj.ptit;

import java.util.Scanner;

/**
 * @author dangquanzt@gmail.com
 */
public class P136SUMC {

	public static int nghichdao(int num) {
		int c = 0;
		while (num != 0) {
			c = c * 10 + num % 10;
			num = num / 10;
		}
		return c;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int a = nghichdao(sc.nextInt());
		int b = nghichdao(sc.nextInt());
		if (a > b) {
			System.out.print(a);
		} else {
			System.out.print(b);
		}
		sc.close();
	}
}
