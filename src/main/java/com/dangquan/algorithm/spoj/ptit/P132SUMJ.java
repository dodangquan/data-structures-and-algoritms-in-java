package com.dangquan.algorithm.spoj.ptit;

import java.io.BufferedInputStream;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Scanner;

/**
 * @author dangquanzt@gmail.com
 */
public class P132SUMJ {

	private static boolean[] isVisited;
	private static int[] arr;
	private static int n;
	private static PriorityQueue<String> queue;
	private static String number;

	private static class MyComparator implements Comparator<String> {

		@Override
		public int compare(String o1, String o2) {
			return o1.compareTo(o2);
		}

	}

	public static void swap(int[] arr, int i, int j) {
		int tmp = arr[i];
		arr[i] = arr[j];
		arr[j] = tmp;
	}

	public static void permute(int[] arr, int i) {
		if (i == n) {
			for (int j = 0; j < n; j++) {
				System.out.print(arr[j]);
			}
			System.out.println();
			return;
		}
		for (int j = i; j < n; j++) {
			swap(arr, i, j);
			permute(arr, i + 1); // recurse call
			swap(arr, i, j); // backtracking
		}
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(new BufferedInputStream(System.in));
		number = sc.next();
		queue = new PriorityQueue<>(new MyComparator());
		arr = new int[number.length()];
		isVisited = new boolean[number.length()];
		permutation2(0);
		if (!queue.isEmpty()) {
			System.out.print(queue.poll());
		} else {
			System.out.print(0);
		}
		sc.close();
	}

	public static void addToQueue() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < arr.length; i++) {
			sb.append(number.charAt(arr[i]));
		}
		if (sb.toString().compareTo(number) > 0) {
			queue.add(sb.toString());
		}
	}

	public static void permutation2(int i) {
		for (int j = 0; j < isVisited.length; j++) {
			if (!isVisited[j]) {
				arr[i] = j;
				isVisited[j] = true;
				if (i == arr.length - 1) {
					addToQueue();
				} else {
					permutation2(i + 1);
				}
				isVisited[j] = false;
			}
		}
	}
}
