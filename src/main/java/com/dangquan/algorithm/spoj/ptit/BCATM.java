package com.dangquan.algorithm.spoj.ptit;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author dangquanzt@gmail.com
 */
public class BCATM {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int length = sc.nextInt();
		int price = sc.nextInt();
		int[] values = new int[length];
		int[] ammounts = new int[length];
		for (int i = 0; i < length; i++) {
			values[i] = sc.nextInt();
			ammounts[i] = 1;
		}
		int count = Integer.MAX_VALUE;
		List<Integer[]> results = solutions(values, ammounts, new int[length], price, 0);
		if (results.isEmpty()) {
			System.out.print(-1);
		} else {
			for (Integer[] result : results) {
				if (count > getInt(result)) {
					count = getInt(result);
				}
			}
			System.out.print(count);
		}
		sc.close();
	}

	public static int getInt(Integer[] result) {
		int count = 0;
		for (int i = 0; i < result.length; i++) {
			count = count + result[i];
		}
		return count;
	}

	public static List<Integer[]> solutions(int[] values, int[] ammounts, int[] variation,
			int price, int position) {
		List<Integer[]> list = new ArrayList<>();
		int value = compute(values, variation);
		if (value < price) {
			for (int i = position; i < values.length; i++) {
				if (ammounts[i] > variation[i]) {
					int[] newVariation = variation.clone();
					newVariation[i]++;
					List<Integer[]> newList = solutions(values, ammounts, newVariation, price, i);
					if (newList != null) {
						list.addAll(newList);
					}
				}
			}
		} else if (value == price) {
			list.add(myCopy(variation));
		}
		return list;
	}

	public static int compute(int[] values, int[] variation) {
		int ret = 0;
		for (int i = 0; i < variation.length; i++) {
			ret += values[i] * variation[i];
		}
		return ret;
	}

	public static Integer[] myCopy(int[] arr) {
		Integer[] ret = new Integer[arr.length];
		for (int i = 0; i < arr.length; i++) {
			ret[i] = arr[i];
		}
		return ret;
	}
}
