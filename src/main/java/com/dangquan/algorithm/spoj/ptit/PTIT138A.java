package com.dangquan.algorithm.spoj.ptit;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author dangquanzt@gmail.com
 */
public class PTIT138A {

	public static boolean tamgiacvuong(int a, int b, int c) {
		if (a + b <= c) {
			return false;
		} else if (a + c <= b) {
			return false;
		} else if (b + c <= a) {
			return false;
		}
		int A = (int) Math.pow(a, 2);
		int B = (int) Math.pow(b, 2);
		int C = (int) Math.pow(c, 2);
		if (a == 0 || b == 0 || c == 0) {
			return false;
		} else if (A == B + C) {
			return true;
		} else if (B == A + C) {
			return true;
		} else if (C == A + B) {
			return true;
		}
		return false;
	}

	public static boolean exit(int a, int b, int c) {
		if (a == 0 && b == 0 && c == 0) {
			return true;
		} else {
			return false;
		}
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int a;
		int b;
		int c;
		ArrayList<String> list = new ArrayList<>();
		while (true) {
			a = sc.nextInt();
			b = sc.nextInt();
			c = sc.nextInt();
			if (exit(a, b, c)) {
				break;
			}
			if (tamgiacvuong(a, b, c)) {
				list.add("right");
			} else {
				list.add("wrong");
			}
		}
		for (String s : list) {
			System.out.print(s);
			if (list.indexOf(s) < list.size() - 1) {
				System.out.println();
			}
		}
		sc.close();
	}
}
