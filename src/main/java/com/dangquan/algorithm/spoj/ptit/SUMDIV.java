package com.dangquan.algorithm.spoj.ptit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

/**
 * @author dangquanzt@gmail.com
 */
public class SUMDIV {

	public static boolean isPrime(long num) {
		if (num <= 1) {
			return false;
		} else {
			for (long i = 2; i <= Math.sqrt(num); i++) {
				if (num % i == 0) {
					return false;
				}
			}
			return true;
		}
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		ArrayList<Long> list = new ArrayList<>();
		HashMap<Long, Integer> map = null;
		int n = sc.nextInt();
		long sumDiv = 0;
		long num;
		long numTemp;
		while (n != 0) {
			n--;
			num = sc.nextLong();
			numTemp = num;
			if (isPrime(num)) {
				sumDiv = num + 1;
			} else {
				long i = 2;
				int count = 0;
				map = new HashMap<>();
				while (numTemp != 1) {
					if (isPrime(i) && numTemp % i == 0) {
						count++;
						numTemp = numTemp / i;
					}
					if (count > 0 && numTemp % i != 0) {
						map.put(i, count);
						count = 0;
					} else if (count == 0) {
						i++;
					}
				}
				Set<Long> set = map.keySet();
				Iterator<Long> iterator = set.iterator();
				long mul = 1;
				// N = p1^k1*p2^k2*....*pn^kn
				// --> SumDiv_N = {[p1^(k1+1)-1]/(k1-1)}*...*{[pn^(kn+1)-1]/(kn-1)}
				while (iterator.hasNext()) {
					Long k = iterator.next();
					mul = (long) (mul * ((Math.pow(k, map.get(k) + 1) - 1) / (k - 1)));
				}
				sumDiv = mul;
			}
			list.add(sumDiv);
			sumDiv = 0;
		}
		for (Long l : list) {
			System.out.println(l);
		}
		sc.close();
	}
}
