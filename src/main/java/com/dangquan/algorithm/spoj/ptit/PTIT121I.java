package com.dangquan.algorithm.spoj.ptit;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author dangquanzt@gmail.com
 */
public class PTIT121I {

	public static String concat(String s, char ch, int i) {
		while (i > 0) {
			s = s + ch;
			i--;
		}
		return s;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		ArrayList<String> list = new ArrayList<>();
		int n = sc.nextInt();
		int stt;
		int count = 0;
		String str;
		String s;
		while (n > 0) {
			s = "";
			n--;
			stt = sc.nextInt();
			count = sc.nextInt();
			str = sc.next();
			for (int t = 0; t < str.length(); t++) {
				s = concat(s, str.charAt(t), count);
			}
			list.add(stt + " " + s);
		}
		for (String a : list) {
			System.out.println(a);
		}
		sc.close();
	}
}
