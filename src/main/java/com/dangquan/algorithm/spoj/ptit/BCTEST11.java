package com.dangquan.algorithm.spoj.ptit;

import java.util.Scanner;

/**
 * @author dangquanzt@gmail.com
 */
public class BCTEST11 {

	public static boolean isSoMayman(int number) {
		boolean kt = true;
		int c;
		while (number != 0) {
			c = number % 10;
			number = number / 10;
			if (c != 4 && c != 7) {
				kt = false;
				break;
			}
		}
		return kt;
	}

	public static boolean isPrime(int num) {
		if (num < 2) {
			return false;
		} else {
			for (int i = 2; i <= Math.sqrt(num); i++) {
				if (num % i == 0) {
					return false;
				}
			}
			return true;
		}
	}

	public static boolean isSochiaMayman(int number) {
		for (int i = 1; i < number; i++) {
			if (isSoMayman(i) && number % i == 0) {
				return true;
			}
		}
		return false;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int number = sc.nextInt();
		if (number < 4) {
			System.out.print("NO");
		} else if (isSoMayman(number)) {
			System.out.print("YES");
		} else if (isPrime(number)) {
			System.out.print("NO");
		} else if (isSochiaMayman(number)) {
			System.out.print("YES");
		} else {
			System.out.print("NO");
		}
		sc.close();
	}
}
