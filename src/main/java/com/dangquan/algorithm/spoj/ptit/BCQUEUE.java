package com.dangquan.algorithm.spoj.ptit;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;
import java.util.Scanner;

/**
 * @author dangquanzt@gmail.com
 */
public class BCQUEUE {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		ArrayList<Integer> list = new ArrayList<>();
		int n = sc.nextInt();
		Queue<String> stringQueue = new ArrayDeque<>();
		while (n > 0) {
			int c = sc.nextInt();
			switch (c) {
				case 1:
					stringQueue.add(list.size() + "");
					n--;
					break;
				case 2:
					if (list.isEmpty()) {
						stringQueue.add("YES");
					} else {
						stringQueue.add("NO");
					}
					n--;
					break;
				case 3:
					int num = sc.nextInt();
					list.add(num);
					n--;
					break;
				case 4:
					if (!list.isEmpty()) {
						list.remove(0);
					}
					n--;
					break;
				case 5:
					if (list.isEmpty()) {
						stringQueue.add("-1");
					} else {
						stringQueue.add(list.get(0) + "");
					}
					n--;
					break;
				case 6:
					if (list.isEmpty()) {
						stringQueue.add("-1");
					} else {
						stringQueue.add(list.get(list.size() - 1) + "");
					}
					n--;
					break;
				default:
					n--;
					break;
			}
		}
		while(!stringQueue.isEmpty()){
			System.out.println(stringQueue.poll());
		}
		sc.close();
	}
}
