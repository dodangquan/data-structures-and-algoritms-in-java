package com.dangquan.algorithm.spoj.ptit;

import java.util.Scanner;
import java.util.Stack;

/**
 * @author dangquanzt@gmail.com
 */
public class ALGOPRO4 {

	private static long heNhiphanHeThapphan(String nhiphanStr) {
		long s = 0;
		for (int i = 0; i < nhiphanStr.length(); i++) {
			s = s + (long) (Long.parseLong(nhiphanStr.charAt(i) + "")
					* Math.pow(2, nhiphanStr.length() - 1 - i));
		}
		return s;
	}

	private static String heThapphanHeBatphan(long num) {
		Stack<String> theStack = new Stack<>();
		String str;
		if (num == 0) {
			return "0";
		}
		while (num != 0) {
			str = "" + (num % 8);
			num = num / 8;
			theStack.add(str);
		}
		str = "";
		while (!theStack.isEmpty()) {
			str = str + theStack.pop();
		}
		return str;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String str = sc.next();
		System.out.print(heThapphanHeBatphan(heNhiphanHeThapphan(str)));
		sc.close();
	}
}
