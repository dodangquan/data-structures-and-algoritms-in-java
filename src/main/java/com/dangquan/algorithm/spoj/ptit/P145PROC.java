package com.dangquan.algorithm.spoj.ptit;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * @author dangquanzt@gmail.com
 */
public class P145PROC {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Set<Integer> set = new HashSet<>();
		for (int i = 0; i < 10; i++) {
			set.add(sc.nextInt() % 42);
		}
		System.out.print(set.size());
		sc.close();
	}
}
