package com.dangquan.algorithm.spoj.ptit;

import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.Queue;
import java.util.Scanner;

/**
 * @author dangquanzt@gmail.com
 */
public class BCGCD {

	public static void main(String[] args) throws Exception {
		Scanner sc = new Scanner(System.in);
		Queue<String> queue = new ArrayDeque<>();
		long num1;
		long num2;
		while (true) {
			num1 = sc.nextLong();
			num2 = sc.nextLong();
			if (num1 == 0 && num2 == 0) {
				break;
			}
			queue.add(ucln(num1, num2) + " " + bcnn(num1, num2));
		}
		Iterator<String> iterator = queue.iterator();
		while(iterator.hasNext()){
			System.out.print(iterator.next());
			if (iterator.hasNext()){
				System.out.println();
			}
		}
		sc.close();
	}

	public static long ucln(long num1, long num2) {
		if (num2 == 0) {
			return num1;
		} else {
			return ucln(num2, num1 % num2);
		}
	}

	public static long bcnn(long num1, long num2) {
		return num1 * num2 / ucln(num1, num2);
	}
}
