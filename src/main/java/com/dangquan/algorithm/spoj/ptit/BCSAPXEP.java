package com.dangquan.algorithm.spoj.ptit;

import java.util.Scanner;

/**
 * @author dangquanzt@gmail.com
 */
public class BCSAPXEP {

	public static void quickSort(int[] a, int low, int high) {
		if (a == null || a.length == 0) {
			return;
		}
		if (low >= high) {
			return;
		}
		int middle = (low + high) / 2;
		int p = a[middle];

		int i = low;
		int j = high;

		while (i <= j) {
			while (a[i] < p) {
				i++;
			}
			while (a[j] > p) {
				j--;
			}
			if (i <= j) {
				int temp = a[j];
				a[j] = a[i];
				a[i] = temp;
				i++;
				j--;
			}
		}
		if (low < j) {
			quickSort(a, low, j);
		}
		if (i < high) {
			quickSort(a, i, high);
		}
	}

	public static void show(int[] a) {
		for (int i = 0; i < a.length; i++) {
			System.out.println(a[i]);
		}
	}

	public static void main(String[] args) throws Exception {
		Scanner sc = new Scanner(System.in);
		int length = sc.nextInt();
		int[] arr = new int[length];
		for (int i = 0; i < length; i++) {
			if (sc.hasNextInt()) {
				arr[i] = sc.nextInt();
			}
		}
		quickSort(arr, 0, length - 1);
		show(arr);
		sc.close();
	}
}
