package com.dangquan.algorithm.spoj.ptit;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author dangquanzt@gmail.com
 */
public class BCPALIN {

	public static boolean soDoixung(int num) {
		int c = num;
		int s = 0;
		if (num < 0){
			return false;
		}
		while (num != 0) {
			s = s * 10;
			s = s + num % 10;
			num = num / 10;
		}
		if (s == c) {
			return true;
		}
		return false;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		ArrayList<String> list = new ArrayList<>();
		int count = sc.nextInt();
		while (count != 0) {
			count--;
			if (soDoixung(sc.nextInt())) {
				list.add("YES");
			} else {
				list.add("NO");
			}
		}
		for (String str : list) {
			System.out.println(str);
		}
		sc.close();
	}
}
