package com.dangquan.algorithm.spoj.ptit;

import java.util.Scanner;

/**
 * @author dangquanzt@gmail.com
 */
public class PTIT016C {

	public static boolean soChanLe(int number) {
		if (number % 2 == 0) {
			return true;
		}
		return false; // so le
	}

	public static String[] doSomething(String[] arrString) {
		String[] arrResult = new String[arrString.length];
		boolean kt = true;
		for (int i = 0; i < arrString.length; i++) {
			for (int j = 0; j < arrString[i].length(); j++) {
				if (!soChanLe(j + 1) && soChanLe(Integer.parseInt(arrString[i].charAt(j) + ""))
						|| soChanLe(j + 1)
								&& !soChanLe(Integer.parseInt(arrString[i].charAt(j) + ""))) {
					kt = false;
					break;
				}
			}
			if (kt) {
				arrResult[i] = "YES";
			} else {
				arrResult[i] = "NO";
				kt = true;
			}
		}
		return arrResult;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		String[] arrString = new String[n];
		for (int i = 0; i < n; i++) {
			arrString[i] = sc.next();
		}
		String[] arrResult = doSomething(arrString);
		for (int i = 0; i < arrResult.length; i++) {
			System.out.print(arrResult[i]);
			if (i < arrResult.length - 1) {
				System.out.println();
			}
		}
		sc.close();
	}
}
