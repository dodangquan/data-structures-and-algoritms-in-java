package com.dangquan.algorithm.stack;

/**
 * @author dangquanzt@gmail.com
 */
public interface Iterator<Item> {

	boolean hasNext();

	Item next();
	
	void remove();
}
