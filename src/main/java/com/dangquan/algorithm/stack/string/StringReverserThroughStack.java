package com.dangquan.algorithm.stack.string;

import com.dangquan.algorithm.stack.Stack;

/**
 * @author dangquanzt@gmail.com
 */
public class StringReverserThroughStack {

	private static String input;
	
	public StringReverserThroughStack() {
		super();
	}
	
	public StringReverserThroughStack(String input){
		StringReverserThroughStack.input = input;
	}
	
	public static void main(String[] args) {
		input = "Viet Nam tuoi dep!";
		String str = "test";
		System.out.println(doReverser(input));
		System.out.println(str.concat(input));
	}
	
	public static String doReverser(String input){
		Stack<String> theStack = new Stack<>();
		for (int i = 0; i < input.length(); i++) {
			String ch = String.valueOf(input.charAt(i));
			theStack.push(ch);
		}
		StringBuilder sb = new StringBuilder();
		while (!theStack.isEmpty()){
			sb.append(theStack.pop());
		}
		return sb.toString();
	}
}
