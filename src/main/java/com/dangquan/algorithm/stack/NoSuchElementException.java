package com.dangquan.algorithm.stack;

/**
 * @author dangquanzt@gmail.com
 */
public class NoSuchElementException extends RuntimeException {

	private static final long serialVersionUID = -7899432185615896165L;
	
	public NoSuchElementException() {
		super();
	}
	
	public NoSuchElementException(String message) {
		super(message);
	}

}
