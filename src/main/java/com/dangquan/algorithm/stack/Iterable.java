package com.dangquan.algorithm.stack;

/**
 * @author dangquanzt@gmail.com
 */
public interface Iterable<Item> {

	Iterator<Item> iterator();
}
