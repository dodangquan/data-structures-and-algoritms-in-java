package com.dangquan.algorithm.stack;

import java.io.Serializable;

/**
 * @author dangquanzt@gmail.com
 */
public class Stack<Item> implements Iterable<Item>, Serializable {

	private static final long serialVersionUID = 5103511992466401169L;
	
	private int size = -1;
	private Node nodeFirst = null;

	private class Node {
		private Item item;
		private Node next;
	}

	public Stack() {
		this.nodeFirst = null;
		this.size = 0;
	}

	public boolean isEmpty() {
		if (nodeFirst == null || size == 0) {
			return true;
		} else {
			return false;
		}
	}

	public int size() {
		if (size >= 0) {
			return size;
		} else {
			throw new NullPointerException();
		}
	}

	public synchronized void push(Item item) {
		Node oldFirst = nodeFirst;
		nodeFirst = new Node();
		nodeFirst.item = item;
		nodeFirst.next = oldFirst;
		size++;
	}

	public synchronized Item pop() {
		if (isEmpty()) {
			throw new NoSuchElementException();
		} else {
			Item item = nodeFirst.item;
			nodeFirst = nodeFirst.next;
			size = size - 1;
			return item;
		}
	}

	public Item peek() {
		if (isEmpty()) {
			throw new NoSuchElementException();
		} else {
			return nodeFirst.item;
		}
	}

	@Override
	public Iterator<Item> iterator() {
		return new StackIterator();
	}

	private class StackIterator implements Iterator<Item> {

		private Node currentNode;

		public StackIterator() {
			StackIterator.this.currentNode = Stack.this.nodeFirst;
		}

		@Override
		public boolean hasNext() {
			if (currentNode == null) {
				return false;
			} else {
				return true;
			}
		}

		@Override
		public Item next() {
			if (!hasNext()) {
				throw new NoSuchElementException();
			} else {
				Item item = StackIterator.this.currentNode.item;
				StackIterator.this.currentNode = StackIterator.this.currentNode.next;
				return item;
			}
		}

		@Override
		public void remove() {
			if (!hasNext()) {
				throw new NoSuchElementException();
			} else {
				Stack.this.nodeFirst = Stack.this.nodeFirst.next;
				size--;
			}
		}

	}
}
