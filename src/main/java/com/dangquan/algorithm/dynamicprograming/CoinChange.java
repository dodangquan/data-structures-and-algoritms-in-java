package com.dangquan.algorithm.dynamicprograming;

/**
 * @author dangquanzt@gmail.com
 */
public class CoinChange {

	public static int dynamic(int[] coins, int amount) {
		if (amount < 0) {
			return 0;
		}
		int[][] solution = new int[coins.length + 1][amount + 1];
		for (int i = 0; i <= coins.length; i++) {
			solution[i][0] = 1;
		}
		for (int i = 1; i <= amount; i++) {
			solution[0][i] = 0;
		}

		for (int i = 1; i <= coins.length; i++) {
			for (int j = 1; j <= amount; j++) {
				if (coins[i - 1] <= j) {
					solution[i][j] = solution[i - 1][j]
							+ solution[i][j - coins[i - 1]];
				} else {
					solution[i][j] = solution[i - 1][j];
				}
			}
		}
		return solution[coins.length][amount];
	}

	public static void main(String[] args) {
		int amount = 4;
		int[] coin = { 5, 1, 2 };
		System.out.println("By Dynamic Programming " + dynamic(coin, amount));
	}
}
