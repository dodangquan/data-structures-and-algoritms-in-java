package com.dangquan.algorithm.dynamicprograming;

/**
 * @author dangquanzt@gmail.com
 */
public class FibonacciDP {

    public static int badFibonacci(int n) {
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }
        return badFibonacci(n - 1) + badFibonacci(n - 2);
    }

    public static long bestFibonacciDP(int num) {
        long n1 = 1;
        long n2 = 1;
        long current = 1;
        for (int i = 3; i <= num; i++) {
            current = n1 + n2;
            n2 = n1;
            n1 = current;
        }
        return current;
    }

    public static void main(String[] args) {
        long t1 = System.currentTimeMillis();
        System.out.println(bestFibonacciDP(40));
        System.out.println(System.currentTimeMillis() - t1);
    }
}
