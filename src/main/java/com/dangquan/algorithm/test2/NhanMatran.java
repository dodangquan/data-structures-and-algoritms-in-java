package com.dangquan.algorithm.test2;

/**
 * @author dangquanzt@gmail.com
 */
public class NhanMatran {

	public static void show(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				System.out.print(matrix[i][j]);
				if (j < matrix[0].length - 1) {
					System.out.print(" ");
				}
			}
			if (i < matrix.length - 1) {
				System.out.println();
			}
		}
	}

	public static void main(String[] args) {
		// int[][] a = { { 1, 0 }, { 1, 0 }, { 1, 0 } };
		// int[][] b = { { 1, 1, 1 }, { 1, 1, 1 } };
//		int[][] c = multiplication(a, b);
		int[][] d = { { 1, 1 }, { 1, 1 } };
		int[][] e = powMatrix(d, 3);
		show(e);
	}

	public static int[][] powMatrix(int[][] matrix, int n) {
		if (n == 2) {
			return multiplication(matrix, matrix);
		} else if (n % 2 == 0 && n > 2) {
			return powMatrix(powMatrix(matrix, n / 2), 2);
		} else if (n % 2 == 1 && n > 2) {
			return multiplication(powMatrix(powMatrix(matrix, n / 2), 2), matrix);
		} else {
			return matrix;
		}
	}

	public static int[][] multiplication(int[][] a, int[][] b) {
		int rowA = a.length;
		int colA = a[0].length;
		int colB = b[0].length;
		int[][] c = new int[rowA][colB];
		int sum = 0;
		// sum = a[0][0] * b[0][0] + a[0][1] * b[1][0] + a[0][2] * b[2][0];
		for (int i = 0; i < rowA; i++) {
			for (int j = 0; j < colB; j++) {
				for (int t = 0; t < colA; t++) {
					sum = sum + a[i][t] * b[t][j];
				}
				c[i][j] = sum;
				sum = 0;
			}
		}
		return c;
	}
}
