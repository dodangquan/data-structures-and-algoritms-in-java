package com.dangquan.algorithm.test2;

import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * @author dangquanzt@gmail.com
 */
public class StringLengthComaparator implements Comparator<String> {
	
	public static void main(String[] args) {
		Comparator<String> comparator = new StringLengthComaparator();
		PriorityQueue<String> priorityQueue = new PriorityQueue<String>(comparator);
		priorityQueue.add("short");
		priorityQueue.add("very long string");
		priorityQueue.add("medium");
		while (!priorityQueue.isEmpty()){
			System.out.println(priorityQueue.remove());
		}
	}

	@Override
	public int compare(String o1, String o2) {
		if (o1.length() < o2.length()) {
			return -1;
		}
		if (o1.length() > o2.length()) {
			return 1;
		}
		return 0;
	}

}
