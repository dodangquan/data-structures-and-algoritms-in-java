package com.dangquan.algorithm.queue;

import java.io.Serializable;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * @author dangquanzt@gmail.com
 */
public class Queue<Item> implements Iterable<Item>, Serializable {
	private static final long serialVersionUID = -214267605070630310L;

	private int size;
	private Node nodeFirst;
	private Node nodeLast;

	public Queue() {
		super();
		nodeFirst = null;
		nodeLast = null;
		size = 0;
	}

	public boolean isEmpty() {
		if (nodeFirst == null) {
			return true;
		} else {
			return false;
		}
	}

	public int size() {
		return size;
	}

	public Item peek() {
		if (isEmpty()) {
			return null;
		} else {
			return nodeFirst.item;
		}
	}

	public Item getLast() {
		if (isEmpty()) {
			throw new NoSuchElementException();
		} else {
			return nodeLast.item;
		}
	}

	public Item getFirst() {
		if (isEmpty()) {
			throw new NoSuchElementException();
		} else {
			return nodeFirst.item;
		}
	}

	public Item dequeue() {
		if (isEmpty()) {
			return null;
		} else {
			Item item = nodeFirst.item;
			nodeFirst = nodeFirst.next;
			size--;
			return item;
		}
	}

	public void enqueue(Item item) {
		Node old = nodeLast;
		nodeLast = new Node();
		nodeLast.item = item;
		nodeLast.next = null;
		if (isEmpty()) {
			nodeFirst = nodeLast;
		} else {
			old.next = nodeLast;
		}
		size++;
	}

	private class Node {
		private Item item;
		private Node next;
	}

	@Override
	public Iterator<Item> iterator() {
		return new MyIterator();
	}

	private class MyIterator implements Iterator<Item> {

		private Node currentNode = Queue.this.nodeFirst;

		@Override
		public boolean hasNext() {
			if (currentNode != null) {
				return true;
			}
			return false;
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}

		@Override
		public Item next() {
			if (!hasNext()) {
				throw new NoSuchElementException();
			} else {
				Item item = MyIterator.this.currentNode.item;
				MyIterator.this.currentNode = MyIterator.this.currentNode.next;
				return item;
			}
		}
	}
}
