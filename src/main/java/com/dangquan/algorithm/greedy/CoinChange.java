package com.dangquan.algorithm.greedy;

/**
 * @author dangquanzt@gmail.com
 */
public class CoinChange {

	public static int minCoins(int coins[], int m, int amount) {
		// base case
		if (amount <= 0)
			return 0;

		// Initialize result
		int result = Integer.MAX_VALUE;

		// Try every coin that has smaller value than V
		for (int i = 0; i < m; i++) {
			if (coins[i] <= amount) {
				int subResult = minCoins(coins, m, amount - coins[i]);

				// Check for INT_MAX to avoid overflow and see if
				// result can minimized
				if (subResult != Integer.MAX_VALUE && subResult + 1 < result)
					result = subResult + 1;
			}
		}
		return result;
	}

	public static void main(String args[]) {
		int coins[] = { 5, 2, 1 };
		int m = coins.length;
		int amount = 30;
		System.out.println("Minimum coins required is " + minCoins(coins, m, amount));
	}
}
