package com.dangquan.algorithm.sort;

/**
 * @author dangquanzt@gmail.com
 */
public class SelectionSort {
	public static void selectionSort2(int[] arr) {
		int min;
		for (int i = arr.length - 1; i > 0; i--) {
			min = 0;
			for (int j = 1; j <= i; j++) {
				if (arr[min] > arr[j]) {
					min = j;
				}
			}
			swap(arr, min, i);
		}
	}

	public static void selectionSort(int[] arr) {
		int min;
		for (int i = 0; i < arr.length; i++) {
			min = i;
			for (int j = i + 1; j < arr.length; j++) {
				if (arr[min] > arr[j]) {
					min = j;
				}
			}
			swap(arr, min, i);
		}
	}

	public static void swap(int[] arr, int i, int j) {
		if (i != j && i >= 0 && j >= 0) {
			int temp = arr[i];
			arr[i] = arr[j];
			arr[j] = temp;
		}
	}

	public static boolean isSorted(int[] array) {
		for (int i = 1; i < array.length; i++) {
			if (array[i] < array[i - 1]) {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		int[] arr = { 6, 4, 5, 10, 2, 3, 7, 1, 9, 8 };
		selectionSort(arr);
		System.out.println(isSorted(arr));
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
	}
}
