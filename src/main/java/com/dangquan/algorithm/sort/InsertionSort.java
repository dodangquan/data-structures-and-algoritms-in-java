package com.dangquan.algorithm.sort;

/**
 * @author dangquanzt@gmail.com
 */
public class InsertionSort {
	public static void insertionSort(int[] arr) {
		int last;
		int j;
		for (int i = 1; i < arr.length; i++) {
			last = arr[i];
			j = i;
			while (j > 0 && arr[j - 1] > last) {
				arr[j] = arr[j - 1];
				j--;
			}
			arr[j] = last;
		}
	}

	public static boolean isSorted(int[] array) {
		for (int i = 1; i < array.length; i++) {
			if (array[i] < array[i - 1]) {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		int[] arr = { 6, 4, 5, 10, 2, 3, 7, 1, 9, 8 };
		insertionSort(arr);
		System.out.println(isSorted(arr));
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
	}
}
