package com.dangquan.algorithm.sort;

/**
 * @author dangquanzt@gmail.com
 */
public class HeapSort {

	private static int n;

	public static void makeMaxHeap(int[] arr) {
		n = arr.length - 1;
		for (int i = n / 2; i >= 0; i--) {
			maxHeap(arr, i);
		}
	}

	public static void makeMinHeap(int[] arr) {
		n = arr.length - 1;
		for (int i = n / 2; i >= 0; i--) {
			minHeap(arr, i);
		}
	}

	public static void minHeap(int[] arr, int i) {
		int left = 2 * i;
		int right = left + 1;
		int smallest = i;
		if (n < 0) {
			return;
		}
		if (left <= n && arr[i] > arr[left]) {
			smallest = left;
		}
		if (right <= n && arr[smallest] > arr[right]) {
			smallest = right;
		}
		if (smallest != i) {
			swap(arr, smallest, i);
			minHeap(arr, smallest);
		}
	}

	public static void maxHeap(int[] arr, int i) {
		int left = 2 * i;
		int right = left + 1;
		int largest = i;

		if (n < 0) {
			return;
		}
		if (left <= n && arr[i] < arr[left]) {
			largest = left;
		}
		if (right <= n && arr[largest] < arr[right]) {
			largest = right;
		}
		if (largest != i) {
			swap(arr, largest, i);
			maxHeap(arr, largest);
		}
	}

	public static void swap(int[] a, int i, int j) {
		int temp = a[i];
		a[i] = a[j];
		a[j] = temp;
	}

	public static void sortMaxHeap(int[] a) {
		makeMaxHeap(a);
		System.out.print("Max Heap: ");
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + " ");
		}
		System.out.println();
		for (int i = n; i >= 0; i--) {
			swap(a, 0, i);
			n = n - 1;
			maxHeap(a, 0);
		}
	}
	
	public static void sortMinHeap(int[] a) {
		makeMinHeap(a);
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + " ");
		}
		System.out.println();
		for (int i = n; i >= 0; i--) {
			swap(a, 0, i);
			n = n - 1;
			minHeap(a, 0);
		}
	}

	public static boolean isSorted(int[] array) {
		for (int i = 1; i < array.length; i++) {
			if (array[i] < array[i - 1]) {
				return false;
			}
		}
		return true;
	}
	
	public static void main(String[] args) {
		int[] a = { 6, 4, 5, 10, 2, 3, 7, 1, 9, 8 };
		sortMaxHeap(a);
//		sortMinHeap(a);
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + " ");
		}		
	}
}
