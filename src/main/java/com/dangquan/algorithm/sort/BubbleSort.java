package com.dangquan.algorithm.sort;

/**
 * @author dangquanzt@gmail.com
 */
public class BubbleSort {
	public static void swap(int[] arr, int i, int j) {
		if (i != j && i >= 0 && j >= 0) {
			int temp = arr[i];
			arr[i] = arr[j];
			arr[j] = temp;
		}
	}

	public static void bubbleSort(int[] arr) {
		boolean flag = true;
		while (flag) {
			flag = false;
			for (int i = 0; i < arr.length - 1; i++) {
				if (arr[i] > arr[i + 1]) {
					swap(arr, i, i + 1);
					flag = true;
				}
			}
		}
	}

	public static boolean isSorted(int[] array) {
		for (int i = 1; i < array.length; i++) {
			if (array[i] < array[i - 1]) {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		int[] arr = { 6, 4, 5, 10, 2, 3, 7, 1, 9, 8 };
		bubbleSort(arr);
		System.out.println(isSorted(arr));
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
	}
}
