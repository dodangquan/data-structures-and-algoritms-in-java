package com.dangquan.algorithm.sort;

/**
 * @author dangquanzt@gmail.com
 */
public class MergeSort {
	public static boolean isSorted(int[] a) {
		for (int i = 1; i < a.length; i++) {
			if (a[i] < a[i - 1]) {
				return false;
			}
		}
		return true;
	}

	private static void merge(int[] a, int[] aux, int low, int mid, int hight) {
		int i = low;
		int j = mid;
		for (int k = low; k < hight; k++) {
			if (i == mid) {
				aux[k] = a[j++];
			} else if (j == hight) {
				aux[k] = a[i++];
			} else if (a[j] < a[i]) {
				aux[k] = a[j++];
			} else {
				aux[k] = a[i++];
			}
		}
		for (int k = low; k < hight; k++) {
			a[k] = aux[k];
		}
	}

	public static void sort(int[] a, int[] aux, int low, int hight) {
		if (hight - low <= 1) {
			return;
		}
		int mid = low + (hight - low) / 2;
		sort(a, aux, low, mid);
		sort(a, aux, mid, hight);
		merge(a, aux, low, mid, hight);
	}

	public static void sort(int[] a) {
		int length = a.length;
		int[] aux = new int[length];
		sort(a, aux, 0, length);
	}

	public static void show(int[] a) {
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + " ");
		}
		System.out.println();
	}
}
