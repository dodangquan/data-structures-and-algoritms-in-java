package com.dangquan.algorithm.sort;

/**
 * @author dangquanzt@gmail.com
 */
public class QuickSort {
	public static void quickSort(int[] a, int low, int high) {
		if (a == null || a.length == 0) {
			return;
		}
		if (low >= high) {
			return;
		}
		int middle = (low + high) / 2;
		int p = a[middle];

		int i = low;
		int j = high;

		while (i <= j) {
			while (a[i] < p) {
				i++;
			}
			while (a[j] > p) {
				j--;
			}
			if (i <= j) {
				int temp = a[j];
				a[j] = a[i];
				a[i] = temp;
				i++;
				j--;
			}
		}
		if (low < j) {
			quickSort(a, low, j);
		}
		if (i < high) {
			quickSort(a, i, high);
		}
	}
	public static void main(String[] args) {
		int[] a = {3, 1, 5, 10, 2, 6, 7, 4, 9, 8};
		quickSort(a, 0, a.length - 1);
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + " ");
		}
	}
}
