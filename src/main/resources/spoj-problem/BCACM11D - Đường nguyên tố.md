# BCACM11D - Đường nguyên tố
Cho hai số nguyên tố khác nhau có bốn chữ số. Người ta cho rằng hoàn toàn có thể<br>
biến đổi từ số này thành số kia sau một số bước theo quy tắc: Tại mỗi bước ta chỉ<br>
thay đổi một chữ số trong số trước đó sao cho số tạo được trong mỗi bước đều là một<br>
số nguyên tố có bốn chữ số. Một cách biến đổi như vậy gọi là một “đường nguyên tố”.<br>

Bài toán đặt ra là với một cặp số nguyên tố đầu vào, hãy tính ra số bước của đường<br>
nguyên tố ngắn nhất. Giả sử đầu vào là hai số `1033` và `8179` thì đường nguyên tố ngắn<br>
nhất sẽ có độ dài là `6` với các bước chuyển là:<br>
>1033<br>
>1 **`7`** 33<br>
>**`3`** 733<br>
>373 **`9`**<br>
>37 **`7`** 9<br>
>**`8`** 779<br>
>8179<br>
## **Input**
Dòng đầu tiên ghi số bộ test, không lớn hơn 100. Mỗi bộ test viết trên một dòng bao gồm<br>
hai số nguyên tố có 4 chữ số.
## **Output**
Với mỗi bộ test, in ra màn hình trên một dòng số bước của đường nguyên tố ngắn nhất.
### Example 
>**`Input:`**<br>
>`3`<br>
>`1033 8179`<br>
>`1373 8017`<br>
>`1033 1033`<br>
><br>
>**`Output:`**<br>
>`6`<br>
>`7`<br>
>`0`<br>