# PTIT016D - ACM PTIT 2016 D - Biểu thức
Một dãy gồm n số nguyên không âm a<sub>1</sub>, a<sub>2</sub>,..., a<sub>n</sub> được viết thành một hàng ngang, giữa<br>
hai số liên tiếp có một khoảng trắng, như vậy có tất cả (n­-1) khoảng trắng. Người ta <br>
muốn đặt k dấu cộng và (n-1-k) dấu trừ vào (n­-1) khoảng trắng đó để nhận được một <br>
biểu thức có giá trị lớn nhất.<br>
Ví dụ, với dãy gồm 5 số nguyên 28, 9, 5, 1, 69 và k = 2 thì cách đặt 28+9-5-1+69 là biểu <br>
thức có giá trị lớn nhất.<br>
**Yêu cầu:** Cho dãy gồm n số nguyên không âm a1, a2,..., an và số nguyên dương k, hãy <br>
tìm cách đặt k dấu cộng và (n-1-k) dấu trừ vào (n­-1) khoảng trắng để nhận được một <br>
biểu thức có giá trị lớn nhất.
## **Input**
- Dòng đầu chứa hai số nguyên dương n, k (k < n ≤ 10<sup>5</sup>).
- Dòng thứ hai chứa n số nguyên không âm a<sub>1</sub>, a<sub>2</sub>,..., a<sub>n</sub> (a<sub>n</sub> ≤ 10<sup>6</sup>).
## **Output**
- Một số nguyên là giá trị của biểu thức đạt được.
### Example
> **`Input`**
>

> `5 2`  
> `28 9 5 1 69`  
>

>**`Output`**
>

>`100`  