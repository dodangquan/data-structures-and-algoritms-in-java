package com.dangquan.algorithm.mergesort;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.dangquan.algorithm.sort.MergeSort;

public class MergeSortTest {

	@Test
	public void sortTest() {
		int[] arr = { 10, 1, 2, 6, 4, 5, 8, 9, 7, 3 };
		assertFalse(MergeSort.isSorted(arr));
		MergeSort.sort(arr);
		MergeSort.show(arr);
		assertTrue(MergeSort.isSorted(arr));
	}

}
