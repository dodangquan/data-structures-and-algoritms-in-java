package com.dangquan.algorithm.heapsort;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;

import com.dangquan.algorithm.sort.HeapSort;

public class HeapSortTest {

	@Test
	public void heapSortTest() {
		int[] array = { 6, 4, 5, 10, 2, 3, 7, 1, 9, 8 };
		System.out.println("Input: " + Arrays.toString(array));
		HeapSort.sortMaxHeap(array);
		System.out.print("Output: ");
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		assertTrue(HeapSort.isSorted(array));
	}

}
