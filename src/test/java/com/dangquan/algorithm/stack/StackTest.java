package com.dangquan.algorithm.stack;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class StackTest {

	@Test
	public void isEmptyTest() {
		Stack<Integer> stackTest = new Stack<Integer>();
		assertTrue(stackTest.isEmpty());
		System.out.println(stackTest.pop());
		assertEquals(0, stackTest.size());
	}

	@Test
	public void pushTest() {
		Stack<Integer> stackTest = new Stack<Integer>();
		assertTrue(stackTest.isEmpty());
		stackTest.push(1);
		assertFalse(stackTest.isEmpty());
	}

}
